### Accepted feedback:
- "Enhancement in the visual interface" by Manuel Segimon
    - "I suggest a potential enhancement in the visual interface. Instead of adding a new object below the current one, consider a design that deletes the current one and adds a new one. This way, the page doesn’t get bigger the more objects you look at, improving the user experience."
        - Our front end now should be able to do just that

- "Confusing object selection in browser" by Seyed Reza Sajjadinasab
    - "It is not clear what are the different 0-14 objects. Selecting between them didn't result in any change in the output."
        - We are adding class display for object numbers to the front end

- "install.txt is misleading" by Seyed Reza Sajjadinasab
    - "It is said that the instruction provided is for "A Unix/Linux-based operating system", however, the installation source is a java library from a location in the lab machines. Using it on another Linux system will change "~/.bashrc" and cause problems for further usage of the OS.
    The received error:
    -bash: /ad/eng/opt/java/add_jdk17.sh: No such file or directory"
        - This should be addressed now in the install.txt

- "Ask user what type of object to detect" by Seyed Reza Sajjadinasab
    - "You can specify a few types of objects that are detectable. It is not clear now what type of object is detected running mvn exec:java -Dexec.mainClass="yolo.YOLOTest" -Dexec.classpathScope="test". For example, why books are not detected as objects?" 
        - The class names next to object names should clear this up

### Rejected feedback:
- "Web Interface improvement" by Tejas Thakur Singh
    - "Add tabs or a compact grid view in order to fix the organization issues that come with having multiple views open"
        - Sort of addressed already by Manuel's feedback

- "Java implementation of the GUI" by Seyed Reza Sajjadinasab
    - "It was required to do the majority of the project with Java. I think it's better to provide a Java version for the GUI as well instead of java script GUI."
        - We focused on functionality of the GUI for the purposes of our 3D localization, and javascript is better suited with the necessary libraries to perform this task

- "Showing the original video/picture" by Seyed Reza Sajjadinasab
    - "The original video can be shown in the browser to make it easier to identify the significance of the tool and the process."
        - Our reconstructed dense object map will take this into account. Furthermore, we are going to show the original scenario(s) the VSLAM is performed on during our demo. The final output on the front end is color mapped, so the objects are reletively visible in a 3D display of the scene

- "Window size not adjusted" by Seyed Reza Sajjadinasab
    - "The size of the window is not adjusted while using a browser to view the result."
        - This seemed to be a sececondary issue to our main goal for the project

- "Command line user interface for image and video" by Seyed Reza Sajjadinasab
    - "It will be good if the user can pass the filename to the tool and it does the detection on the specified file."
        - We have scripts available to perform this task on the backend, and we provide 2 RGBD datasets where users can perform VSLAM on for our functional front end. We also provide video captured using a monocular camera source that we can perform VSLAM on, but this dataset is not displayed in the front end due to our algorithms incompatibility with depth-less data

### Additional changes
- Improved object tracking algorithm
- Improved VSLAM by incorportaing VSLAM RGBD
- Ability to perform monocularr VSLAM on footage collected from arbitrary data source
- The biggest issue that was raised was the GUI, due to its lack of clarity. We have completely overhauled the frontend, and improved the backend to provide better accuracy. The following list details how we did this:
    1. Migrated from the THREE.js pointcloud plotting library to Plotly.js
    2. Improved styling of the frontend
