package object_detection;

import org.ejml.data.*;

import java.io.File;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import object_detection.types.*;

import java.io.FileWriter;
import java.io.IOException;

class CameraPoseTests {

    @Test
    void testCameraPoseInitialization() throws IOException {
        // Create a temporary configuration file for testing
        String tempFilePath = "test_pose.csv";
        try (FileWriter writer = new FileWriter(tempFilePath)) {
            writer.write("1.0,2.0,3.0\n"); // Translation vector
            writer.write("1.0,0.0,0.0\n"); // R matrix rows
            writer.write("0.0,1.0,0.0\n");
            writer.write("0.0,0.0,1.0\n");
        }

        // Create a CameraPose object
        CameraPose cp = new CameraPose(tempFilePath);

        // Check the translation vector
        DMatrixRMaj expectedTranslation = new DMatrixRMaj(new double[]{1.0, 2.0, 3.0});
        assertArrayEquals(expectedTranslation.data, cp.getTranslation().data, "Translation vector should be correctly initialized");

        // Check the rotation matrix
        DMatrixRMaj expectedR = new DMatrixRMaj(new double[][]{
            {1.0, 0.0, 0.0},
            {0.0, 1.0, 0.0},
            {0.0, 0.0, 1.0}
        });

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                assertEquals(expectedR.get(i, j), cp.getR().get(i, j), 0.01, "R matrix values should match");
            }
        }

        // Clean up temporary file
        new File(tempFilePath).delete();
    }
}

