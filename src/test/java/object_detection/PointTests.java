package object_detection;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import object_detection.types.*;

class PointTests {

    @Test
    void testPointCreation() {
        // Test the creation of a Point object
        Point point = new Point(1.0f, 2.0f, 3.0f);
        assertNotNull(point, "Point object should not be null");
        assertEquals(1.0f, point.getX(), "X coordinate should match the constructor input");
        assertEquals(2.0f, point.getY(), "Y coordinate should match the constructor input");
        assertEquals(3.0f, point.getZ(), "Z coordinate should match the constructor input");
    }

    @Test
    void testPointCreationWithColor() {
        // Test the creation of a Point object with color attributes
        Point point = new Point(1.0f, 2.0f, 3.0f, 255, 0, 0);
        int[] color = point.getColor();

        assertNotNull(point, "Point object should not be null");
        assertEquals(1.0f, point.getX(), "X coordinate should match the constructor input");
        assertEquals(2.0f, point.getY(), "Y coordinate should match the constructor input");
        assertEquals(3.0f, point.getZ(), "Z coordinate should match the constructor input");

        assertArrayEquals(new int[]{255, 0, 0}, color, "Color should match the constructor input");
    }

    @Test
    void testPointEquality() {
        // Test the equality method under two scenarios
        Point p1 = new Point(1.005f, 2.005f, 3.005f);
        Point p2 = new Point(1.006f, 2.006f, 3.006f);

        assertTrue(Point.equals(p1, p2, 0.01f), "Points p1 and p2 should be considered equal with a tolerance of 0.01");
        assertFalse(Point.equals(p1, p2, 0.001f), "Points p1 and p2 should not be considered equal with a tolerance of 0.001");
    }

    @Test
    void testPrecisionEquality() {
        // Test precision issues and rounding errors
        Point p1 = new Point(0.0000001f, 0.0000001f, 0.0000001f);
        Point p2 = new Point(0.0000002f, 0.0000002f, 0.0000002f);

        assertFalse(Point.equals(p1, p2, 0.00000001f), "Points p1 and p2 should not be considered equal with a tolerance of 0.00000001");
    }

    @Test
    void testNegativeCoordinates() {
        // Test points with negative coordinates to ensure that equality checks are not biased by sign
        Point p1 = new Point(-1.005f, -2.005f, -3.005f);
        Point p2 = new Point(-1.005f, -2.005f, -3.005f);

        assertTrue(Point.equals(p1, p2, 0.01f), "Negative coordinate points should be considered equal");
    }

    @Test
    void testZeroCoordinates() {
        // Test points with all coordinates set to zero
        Point p1 = new Point(0.0f, 0.0f, 0.0f);
        Point p2 = new Point(0.0f, 0.0f, 0.0f);

        assertTrue(Point.equals(p1, p2, 0.0001f), "Zero coordinate points should be exactly equal");
    }

    @Test
    void testDistinctPoints() {
        // Test distinct points that should not be equal
        Point p1 = new Point(1.000f, 1.000f, 1.000f);
        Point p2 = new Point(2.000f, 2.000f, 2.000f);

        assertFalse(Point.equals(p1, p2, 0.001f), "Distinct points should not be considered equal");
    }

    @Test
    void testToString() {
        // Test the toString method of the Point class
        Point point = new Point(1.0f, 2.0f, 3.0f);
        assertEquals("Point(1.0 ,2.0 ,3.0)", point.toString(), "String representation should match expected format");
    }

    @Test
    void testHashCode() {
        // Test the hash code generation for the Point class
        Point p1 = new Point(1.0f, 2.0f, 3.0f);
        Point p2 = new Point(1.0f, 2.0f, 3.0f);
        
        assertEquals(p1.hashCode(), p2.hashCode(), "Hash codes should be identical for points with identical coordinates");
    }
}

