package object_detection;

import org.junit.jupiter.api.BeforeEach;
import object_detection.types.*;

import java.io.FileWriter;
import java.io.IOException;
import java.io.FileNotFoundException;

class FrameTests {

    private CameraPose cameraPose;
    private Frame frame;

    @BeforeEach
    void setUp() throws IOException, FileNotFoundException {
        // Create a CameraPose for the frame
        String tempPoseFile = "test_pose.csv";
        try (FileWriter writer = new FileWriter(tempPoseFile)) {
            writer.write("1.0,2.0,3.0\n"); // Translation vector
            writer.write("1.0,0.0,0.0\n"); // R matrix rows
            writer.write("0.0,1.0,0.0\n");
            writer.write("0.0,0.0,1.0\n");
        }
        cameraPose = new CameraPose(tempPoseFile);

        // Create a temporary bbox file for the frame
        String tempBboxFile = "test_bbox.csv";
        try (FileWriter writer = new FileWriter(tempBboxFile)) {
            writer.write("Class,x,y,w,h\n"); // Header
            writer.write("vehicle,10,10,20,20\n");
            writer.write("animal,50,50,30,30\n");
        }

        // Initialize the Frame object
        frame = new Frame(tempBboxFile, cameraPose);
    }
}
